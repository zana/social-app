from django.shortcuts import render

def index(request):
    return render(request, 'baseapp/pages/index.html')